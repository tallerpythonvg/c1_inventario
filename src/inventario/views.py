from django.shortcuts import render
from .forms import RegFormInventario
from .models import Inventariado

# Create your views here.
def inventario(request):
    form = RegFormInventario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombreproducto = form_data.get("nombre_producto")
        cantidadproducto = form_data.get("cantidad_producto")
        pertenencia1 = form_data.get("pertenencia_1")
        pertenencia2 = form_data.get("pertenencia_2")
        ingreso = Inventariado.objects.create(nombre_producto=nombreproducto, cantidad_producto=cantidadproducto, pertenencia_1=pertenencia1, pertenencia_2=pertenencia2)


    contexto = {
        "formulario_inv": form,
    }
    return render(request, "inventario.html", contexto)

