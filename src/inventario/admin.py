from django.contrib import admin
from .models import Inventariado
from .forms import RegModelFormInventario

class AdminInventariados(admin.ModelAdmin):
    list_filter = ["pertenencia_1"]
    form = RegModelFormInventario
    search_fields = ["nombre_producto", "pertenencia_1", "pertenencia_2"]

    #class Meta:
    #   model = Inventariado

# Register your models here.
admin.site.register(Inventariado, AdminInventariados)



