from django.db import models

# Create your models here.
class Inventariado(models.Model):
    id_inventariado = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, null=False, blank=False)
    cantidad_producto = models.IntegerField()
    pertenencia_1 = models.CharField(max_length=100, null=False, blank=False)
    pertenencia_2 = models.CharField(max_length=100, null=False, blank=False)

    objects = models.Manager()

    def __str__(self):
        return self.nombre_producto

    class Meta:
        verbose_name = 'Inventario'
        verbose_name_plural = 'Productos Inventariados'