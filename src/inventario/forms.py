from django import forms
from .models import Inventariado

class RegModelFormInventario(forms.ModelForm):
    class Meta:
        modelo = Inventariado
        campos = ["nombre_producto", "cantidad_producto", "pertenencia_1", "pertenencia_2"]

    def clean_nombre_producto(self): # 'nombre_producto' --> Debe ser igual al nombre del atributo de la clase 'Inventariado'
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_cantidad_producto(self):
        cantidadproducto = self.cleaned_data.get("cantidad_producto")
        if cantidadproducto:
            if cantidadproducto <= 0:
                raise forms.ValidationError("El campo cantidad producto no puede ser negativo")
            return cantidadproducto
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_pertenencia_1(self):
        pertenencia1 = self.cleaned_data.get("pertenencia_1")
        if pertenencia1:
            if len(pertenencia1) <= 1:
                raise forms.ValidationError("El campo pertenencia 1 no puede ser de un caracter")
            return pertenencia1
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_pertenencia_2(self):
        pertenencia2 = self.cleaned_data.get("pertenencia_2")
        if pertenencia2:
            if len(pertenencia2) <= 1:
                raise forms.ValidationError("El campo pertenencia 2 no puede ser de un caracter")
            return pertenencia2
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegFormInventario(forms.Form):  # Las variables deben ser igual a los nombres en el models.py
    nombre_producto = forms.CharField(max_length=100)
    cantidad_producto = forms.IntegerField()
    pertenencia_1 = forms.CharField(max_length=100)
    pertenencia_2 = forms.CharField(max_length=100)

    def clean_nombre_producto(self): # 'nombre_producto' --> Debe ser igual al nombre del atributo de la clase 'Inventariado'
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_cantidad_producto(self):
        cantidadproducto = self.cleaned_data.get("cantidad_producto")
        if cantidadproducto:
            if cantidadproducto <= 0:
                raise forms.ValidationError("El campo cantidad producto no puede ser negativo")
            return cantidadproducto
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_pertenencia_1(self):
        pertenencia1 = self.cleaned_data.get("pertenencia_1")
        if pertenencia1:
            if len(pertenencia1) <= 1:
                raise forms.ValidationError("El campo pertenencia 1 no puede ser de un caracter")
            return pertenencia1
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_pertenencia_2(self):
        pertenencia2 = self.cleaned_data.get("pertenencia_2")
        if pertenencia2:
            if len(pertenencia2) <= 1:
                raise forms.ValidationError("El campo pertenencia 2 no puede ser de un caracter")
            return pertenencia2
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")



