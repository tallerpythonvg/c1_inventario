from django import forms
from .models import ElProducto

class ModelFormProducto(forms.ModelForm):
    class Meta:
        modelo = ElProducto
        campos = ["nombre_producto", "descripcion_producto", "categoria_producto", "sub_categoria_producto"]

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_descripcion_producto(self):
        descripcion = self.cleaned_data.get("descripcion_producto")
        if descripcion:
            if len(descripcion) <= 1:
                raise forms.ValidationError("El campo descripcion producto no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_categoria_producto(self):
        categoria = self.cleaned_data.get("categoria_producto")
        if categoria:
            if len(categoria) <= 1:
                raise forms.ValidationError("El campo categoria producto no puede ser de un caracter")
            return categoria
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_sub_categoria_producto(self):
        sub_categoria = self.cleaned_data.get("sub_categoria_producto")
        if sub_categoria:
            if len(sub_categoria) <= 1:
                raise forms.ValidationError("El campo sub_categoria producto no puede ser de un caracter")
            return sub_categoria
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class FormProducto(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    descripcion_producto = forms.CharField(max_length=100)
    categoria_producto = forms.CharField(max_length=100)
    sub_categoria_producto = forms.CharField(max_length=100)

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_descripcion_producto(self):
        descripcion = self.cleaned_data.get("descripcion_producto")
        if descripcion:
            if len(descripcion) <= 1:
                raise forms.ValidationError("El campo descripcion producto no puede ser de un caracter")
            return descripcion
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_categoria_producto(self):
        categoria = self.cleaned_data.get("categoria_producto")
        if categoria:
            if len(categoria) <= 1:
                raise forms.ValidationError("El campo categoria producto no puede ser de un caracter")
            return categoria
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_sub_categoria_producto(self):
        sub_categoria = self.cleaned_data.get("sub_categoria_producto")
        if sub_categoria:
            if len(sub_categoria) <= 1:
                raise forms.ValidationError("El campo sub_categoria producto no puede ser de un caracter")
            return sub_categoria
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")