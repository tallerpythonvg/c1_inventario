from django.db import models

# Create your models here.
class ElProducto(models.Model):
    id_producto = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, null=False, blank=False)
    descripcion_producto = models.CharField(max_length=100, null=False, blank=False)
    categoria_producto = models.CharField(max_length=100, null=False, blank=False)
    sub_categoria_producto = models.CharField(max_length=100, null=False, blank=False)

    objects = models.Manager

    def __str__(self):
        return self.nombre_producto

    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Tipos de Productos'
