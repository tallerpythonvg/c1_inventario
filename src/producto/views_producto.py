from django.shortcuts import render
from .forms import FormProducto
from .models import ElProducto

# Create your views here.
def producto(request):
    form = FormProducto(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombreproducto = form_data.get("nombre_producto")
        descripcionproducto = form_data.get("descripcion_producto")
        categoriaproducto = form_data.get("categoria_producto")
        subcategoriaproducto = form_data.get("sub_categoria_producto")
        ingreso = ElProducto.objects.create(nombre_producto=nombreproducto, descripcion_producto=descripcionproducto, categoria_producto=categoriaproducto, sub_categoria_producto=subcategoriaproducto)

    contexto = {
        "formulario_pro": form,
    }
    return render(request, "producto.html", contexto)

