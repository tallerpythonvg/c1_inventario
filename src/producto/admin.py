from django.contrib import admin
from .models import ElProducto
from .forms import ModelFormProducto

# Register your models here.
class AdminProducto(admin.ModelAdmin):
    list_filter = ["nombre_producto", "categoria_producto"]
    form = ModelFormProducto
    search_fields = ["nombre_producto", "categoria_producto"]


admin.site.register(ElProducto, AdminProducto)
