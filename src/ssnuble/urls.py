"""ssnuble URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from inventario import views
from bodega import views_bodega
from adquisiciones import views_adquisiciones
from producto import views_producto

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^inventario/$', views.inventario, name='inventario'),
    url(r'^bodega/$', views_bodega.bodega, name='bodega'),
    url(r'^adquisiciones/$', views_adquisiciones.adquisiciones, name='adquisiciones'),
    url(r'^producto/$', views_producto.producto, name='producto'),
]
