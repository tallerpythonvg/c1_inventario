from django.contrib import admin
from .models import Adquisicion
from .forms import ModelFormAdquisicion

# Register your models here.
class AdminAdquisicion(admin.ModelAdmin):
    list_filter = ["nombre_producto", "nombre_proveedor"]
    form = ModelFormAdquisicion
    search_fields = ["nombre_producto", "nombre_proveedor", "numero_factura"]

admin.site.register(Adquisicion, AdminAdquisicion)

