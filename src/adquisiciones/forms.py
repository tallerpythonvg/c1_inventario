from django import forms
from .models import Adquisicion

class ModelFormAdquisicion(forms.ModelForm):
    class Meta:
        modelo = Adquisicion
        campos = ["nombre_producto", "nombre_proveedor", "cantidad_adquirida", "fecha_adquisicion", "numero_factura"]

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_nombre_proveedor(self):
        nombre = self.cleaned_data.get("nombre_proveedor")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre proveedor no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_cantidad_adquirida(self):
        cantidad = self.cleaned_data.get("cantidad_adquirida")
        if cantidad:
            if cantidad <= 0:
                raise forms.ValidationError("El campo cantidad adquirida no puede ser negativo")
            return cantidad
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_fecha_adquisicion(self):
        fecha = self.cleaned_data.get("fecha_adquisicion")
        if fecha:
            return fecha
        else:
            raise forms.ValidationError("Debe ingresar una fecha")

    def clean_numero_factura(self):
        numerof = self.cleaned_data.get("numero_factura")
        if numerof:
            if numerof <= 0:
                raise forms.ValidationError("El campo número de factura no puede ser negativo")
            return numerof
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")


class FormAdquisicion(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    nombre_proveedor = forms.CharField(max_length=100)
    cantidad_adquirida = forms.IntegerField()
    fecha_adquisicion = forms.DateField()
    numero_factura = forms.IntegerField()

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_nombre_proveedor(self):
        nombre = self.cleaned_data.get("nombre_proveedor")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre proveedor no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_cantidad_adquirida(self):
        cantidad = self.cleaned_data.get("cantidad_adquirida")
        if cantidad:
            if cantidad <= 0:
                raise forms.ValidationError("El campo cantidad adquirida no puede ser negativo")
            return cantidad
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_fecha_adquisicion(self):
        fecha = self.cleaned_data.get("fecha_adquisicion")
        if fecha:
            return fecha
        else:
            raise forms.ValidationError("Debe ingresar una fecha")

    def clean_numero_factura(self):
        numerof = self.cleaned_data.get("numero_factura")
        if numerof:
            if numerof <= 0:
                raise forms.ValidationError("El campo número de factura no puede ser negativo")
            return numerof
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")