from django.db import models

# Create your models here.
class Adquisicion(models.Model):
    id_adquisicion = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, null=False, blank=False)
    nombre_proveedor = models.CharField(max_length=100, null=False, blank=False)
    cantidad_adquirida = models.PositiveIntegerField(null=False)
    fecha_adquisicion = models.DateField(auto_now_add=False)
    numero_factura = models.PositiveIntegerField(null=False)

    objects = models.Manager

    def __str__(self):
        return self.nombre_producto

    class Meta:
        verbose_name = 'Adquisición'
        verbose_name_plural = 'Productos Adquiridos o comprados'