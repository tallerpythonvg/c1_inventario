from django.shortcuts import render
from .forms import FormAdquisicion
from .models import Adquisicion

# Create your views here.
def adquisiciones(request):
    form = FormAdquisicion(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombreproducto = form_data.get("nombre_producto")
        nombreproveedor = form_data.get("nombre_proveedor")
        cantidadadquirida = form_data.get("cantidad_adquirida")
        fechaadquisicion = form_data.get("fecha_adquisicion")
        numerofactura = form_data.get("numero_factura")
        ingreso = Adquisicion.objects.create(nombre_producto=nombreproducto, nombre_proveedor=nombreproveedor, cantidad_adquirida=cantidadadquirida, fecha_adquisicion=fechaadquisicion, numero_factura=numerofactura)

    contexto = {
        "formulario_adq": form,
    }
    return render(request, "adquisiciones.html", contexto)

