from django.shortcuts import render
from .forms import RegFormBodega
from .models import Bodegaje

# Create your views here.
def bodega(request):
    form = RegFormBodega(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre = form_data.get("nombre_producto")
        mistock = form_data.get("stock")
        mistockminimo = form_data.get("stock_minimo")
        ubicacion = form_data.get("ubicacion_producto")
        ingreso = Bodegaje.objects.create(nombre_producto=nombre, stock=mistock, stock_minimo=mistockminimo, ubicacion_producto=ubicacion)

    contexto = {
        "formulario_bod": form,
    }
    return render(request, "bodega.html", contexto)

