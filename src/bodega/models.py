from django.db import models

# Create your models here.
class Bodegaje(models.Model):
    id_bodega = models.AutoField(primary_key=True, unique=True)
    nombre_producto = models.CharField(max_length=100, null=False, blank=False)
    stock = models.IntegerField()
    stock_minimo = models.IntegerField()
    ubicacion_producto = models.CharField(max_length=100, null=False, blank=False)
    fecha_ingreso_bodega = models.DateTimeField(auto_now_add=True, auto_now=False)

    objects = models.Manager

    def __str__(self):
        return self.nombre_producto

    class Meta:
        verbose_name = 'Ingreso de Bodega'
        verbose_name_plural = 'Ingresos a Bodega'
