from django.contrib import admin
from .models import Bodegaje
from .forms import RegModelFormBodega

# Register your models here.
class AdminBodegajes(admin.ModelAdmin):
    list_filter = ["nombre_producto"]
    form = RegModelFormBodega
    search_fields = ["nombre_producto", "ubicacion_producto"]

admin.site.register(Bodegaje, AdminBodegajes)
