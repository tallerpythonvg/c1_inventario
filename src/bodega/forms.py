from django import forms
from .models import Bodegaje

class RegModelFormBodega(forms.ModelForm):
    class Meta:
        modelo = Bodegaje
        campos = ["nombre_producto", "stock", "stock_minimo", "ubicacion_producto"]

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_stock(self):
        mistock = self.cleaned_data.get("stock")
        if mistock:
            if mistock <= 0:
                raise forms.ValidationError("El campo stock no puede ser negativo")
            return mistock
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_stock_minimo(self):
        mistockminimo = self.cleaned_data.get("stock_minimo")
        if mistockminimo:
            if mistockminimo <= 0:
                raise forms.ValidationError("El campo stock mínimo no puede ser negativo")
            return mistockminimo
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_ubicacion_producto(self):
        ubicacionproducto = self.cleaned_data.get("ubicacion_producto")
        if ubicacionproducto:
            if len(ubicacionproducto) <= 1:
                raise forms.ValidationError("El campo ubicación producto no puede ser de un caracter")
            return ubicacionproducto
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")


class RegFormBodega(forms.Form):
    nombre_producto = forms.CharField(max_length=100)
    stock = forms.IntegerField()
    stock_minimo = forms.IntegerField()
    ubicacion_producto = forms.CharField(max_length=100)

    def clean_nombre_producto(self):
        nombre = self.cleaned_data.get("nombre_producto")
        if nombre:
            if len(nombre) <= 1:
                raise forms.ValidationError("El campo nombre producto no puede ser de un caracter")
            return nombre
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")

    def clean_stock(self):
        mistock = self.cleaned_data.get("stock")
        if mistock:
            if mistock <= 0:
                raise forms.ValidationError("El campo stock no puede ser negativo")
            return mistock
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_stock_minimo(self):
        mistockminimo = self.cleaned_data.get("stock_minimo")
        if mistockminimo:
            if mistockminimo <= 0:
                raise forms.ValidationError("El campo stock mínimo no puede ser negativo")
            return mistockminimo
        else:
            raise forms.ValidationError("Debe ingresar un valor numérico mayor o igual a 1")

    def clean_ubicacion_producto(self):
        ubicacionproducto = self.cleaned_data.get("ubicacion_producto")
        if ubicacionproducto:
            if len(ubicacionproducto) <= 1:
                raise forms.ValidationError("El campo ubicación producto no puede ser de un caracter")
            return ubicacionproducto
        else:
            raise forms.ValidationError("Debe ingresar al menos un caracter")